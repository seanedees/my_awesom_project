
#include "basic_types.h"
#include "lmx.h"
#include "timeout.h"

#include "printf.h"

#define BG_DEBUG

extern HandleType hNetThread;
extern HandleType hTeThread;

static uint32_t FramesPerSec;

static uint32_t APP_BGCounter;

void initAppBackground(void)
{
   FramesPerSec = lmxGetFramesPerSec();
}

void appBackgroundRoutine(uintptr_t DummyArg)
{
   TO_FrameType Timeout;

   PRINTF("BG 0\n");

   toFrameSet(&Timeout, 1);

   while (1)
   {
      if (toFrameExpired(&Timeout))
      {
         toFrameReset(&Timeout);

         APP_BGCounter += 1;

         if ((APP_BGCounter % (FramesPerSec * 10)) == 0)
         {
#ifdef BG_DEBUG
            PRINTF("BG %d\n", APP_BGCounter);
#endif
         }
      }

      lmxSwitchThreads(hNetThread);
      lmxSwitchThreads(hTeThread);
   }
}
